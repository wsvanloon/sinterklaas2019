# Define a list of blocks to use in the game
rotate <- function(x){
  t(x)[,ncol(x):1]
}

createBlock <- function(block_type, field_height=20, field_width=10){
  
  if(!(block_type %in% 1:7)){
    stop("block_type must be one of 1,2, .., 7.")
  }
  
  candidates <- list(matrix(c(0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0), nrow=4, byrow=T),
                     matrix(c(1,0,0,1,1,1,0,0,0), nrow=3, byrow=T),
                     matrix(c(0,0,1,1,1,1,0,0,0), nrow=3, byrow=T),
                     matrix(c(1,1,1,1), nrow=2, byrow=T),
                     matrix(c(0,1,1,1,1,0,0,0,0), nrow=3, byrow=T),
                     matrix(c(0,1,0,1,1,1,0,0,0), nrow=3, byrow=T),
                     matrix(c(1,1,0,0,1,1,0,0,0), nrow=3, byrow=T))
  
  block1 <- block2 <- block3 <- block4 <- matrix(0, nrow=field_height, ncol=field_width)
  
  b1 <- candidates[[block_type]]
  b2 <- rotate(b1)
  b3 <- rotate(b2)
  b4 <- rotate(b3)
  
  
  block1[1:nrow(candidates[[block_type]]), (floor(10/2) - 1):(floor(10/2) - 2 + ncol(candidates[[block_type]]))] <- b1
  block2[1:nrow(candidates[[block_type]]), (floor(10/2) - 1):(floor(10/2) - 2 + ncol(candidates[[block_type]]))] <- b2
  block3[1:nrow(candidates[[block_type]]), (floor(10/2) - 1):(floor(10/2) - 2 + ncol(candidates[[block_type]]))] <- b3
  block4[1:nrow(candidates[[block_type]]), (floor(10/2) - 1):(floor(10/2) - 2 + ncol(candidates[[block_type]]))] <- b4
  
  out <- list(block1=block1, block2=block4, block3=block3, block4=block2, block_type=block_type)
  
  return(out)
}

# block_list <- lapply(c(6,3,7,1,5), createBlock)
# block_list <- lapply(1:7, createBlock)
block_list <- lapply(c(1, 6, 3, 2, 1, 6, 3, 2,
                       4, 7, 7, 2, 3,
                       2, 1, 1, 7, 1, 3, 4,
                       2, 3, 4, 4,
                       7, 7, 1, 2,
                       4, 7, 2, 2,
                       5, 2, 1, 1,
                       4, 6, 7, 6,
                       1, 2, 4, 1, 1, 1), createBlock)
